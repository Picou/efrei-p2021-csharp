﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Shard.API.Models.buildings;
using Shard.Shared.Core;

namespace Shard.API.Models
{
    public class Constructions
    {
        public List<BuildingItem> Buildings { get; set; }

        public Constructions()
        {
            Buildings = new List<BuildingItem>();
        }

        internal void AddABuilding(ReceivedBuilding building, Unit builder, DateTime builtDate)
        {
            BuildingItem newBuilding;
            if (building.Type == "mine")
                newBuilding = new MineItem(building.Type, builder.Id, builder.System, builder.Planet, building.ResourceCategory, builtDate);
            else
                newBuilding = new StarportItem(building.Type, builder.Id, builder.System, builder.Planet, builtDate);
            Buildings.Add(newBuilding);
        }

        internal void BuildStarportAfterTime(StarportItem starportItem, IClock clock)
        {
            TimeSpan finishedBuilding = (starportItem.EstimatedBuildTime ?? clock.Now) - clock.Now;
            clock.CreateTimer(BuildStarport, starportItem, finishedBuilding, TimeSpan.FromMilliseconds(-1));
        }

        internal void BuildStarport(object starport)
        {
            StarportItem finishedStarport = (StarportItem)starport;
            finishedStarport.IsBuilt = true;
            finishedStarport.EstimatedBuildTime = null;
        }

        internal void BuildMineAfterTime(BuildingItem addedBuilding, PlanetItem planetItem, User user, IClock clock)
        {
            MineExtractor mineExtractor = new MineExtractor(addedBuilding as MineItem, planetItem, user, clock);

            BuildingExtractionWhenReady(mineExtractor);
        }

        internal BuildingItem GetABuilding(string type, string system, string planet)
        {
            // Change this if there can be several buildings of the same type on a same planet
            return Buildings
                .Where(building => building.Type == type)
                .Where(building => building.System == system)
                .Where(building => building.Planet == planet)
                .FirstOrDefault();
        }

        internal void BuildingExtractionWhenReady(MineExtractor mineExtractor)
        {
            if (mineExtractor.BuilderHasNotMoved())
            {
                mineExtractor.ActivateMineWhenReady();
            }
        }

        

        internal List<BuildingItem> GetBuildingsOfUser(User user)
        {
            List<Unit> builders = (List<Unit>)user.Units
                .Where(u => u.Type == "builder")
                .Select(u => u)
                .ToList();

            List<BuildingItem> buildings = Buildings
                .Where(b => builders.Any(a => a.Id == b.BuilderId))
                .ToList();

            return buildings;
        }

        internal BuildingItem GetABuilding(User user, string buildingId)
        {
            List<Unit> builders = (List<Unit>)user.Units
                .Where(u => u.Type == "builder")
                .Select(u => u)
                .ToList();

            BuildingItem building = Buildings.FirstOrDefault(b => b.Id == buildingId);
            if (building == null)
                return null;
            foreach(Unit builder in builders)
            {
                if (builder.Id == building.BuilderId)
                    return building;
            }

            return null;
        }

        internal void DeleteCurrentlyBuilt(Unit builder, string planetId, string systemId)
        {
            BuildingItem building = Buildings.Find(building => building.Planet == planetId
                && building.System == systemId
                && building.BuilderId == builder.Id
                && building.EstimatedBuildTime != null);
            if (building != null)
            {
                builder.BuildCanceller.Cancel();
                Buildings.Remove(building);
            }
        }
    }
}
