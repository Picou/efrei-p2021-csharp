﻿using Shard.Shared.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Shard.API.Models
{
    public class MineExtractor
    {
        private readonly MineItem mine;
        private readonly PlanetItem planet;
        private readonly User owner;
        private readonly IClock clock;

        public MineExtractor(MineItem mine, PlanetItem planet, User owner, IClock clock)
        {
            this.mine = mine;
            this.planet = planet;
            this.owner = owner;
            this.clock = clock;
        }

        public void ActivateMineWhenReady()
        {
            TimeSpan finishedBuilding = (mine.EstimatedBuildTime ?? clock.Now) - clock.Now;
            clock.CreateTimer(Build, null, finishedBuilding, TimeSpan.FromMilliseconds(-1));
            clock.CreateTimer(Extract, null, finishedBuilding.Add(TimeSpan.FromMinutes(1)), TimeSpan.FromMinutes(1));
        }

        private void Build(object state)
        {
            mine.IsBuilt = true;
            mine.EstimatedBuildTime = null;
        }

        //FIXME : can surely be redone to something more readable
        public void Extract(object state)
        {

            if (mine.ResourceCategory == "liquid" && planet.ResourceQuantity.ContainsKey("Water") && planet.ResourceQuantity["Water"] > 0)
            {
                planet.ResourceQuantity["Water"]--;
                owner.ResourcesQuantity["water"]++;
            }
            else if (mine.ResourceCategory == "gaseous" && planet.ResourceQuantity.ContainsKey("Oxygen") && planet.ResourceQuantity["Oxygen"] > 0)
            {
                planet.ResourceQuantity["Oxygen"]--;
                owner.ResourcesQuantity["oxygen"]++;
            }
            else if (mine.ResourceCategory == "solid" && planet.ResourceQuantity.Values.Max() > 0)
            {
                Dictionary<string, int> solidsDictionary = planet.ResourceQuantity
                   .Where(resource => resource.Key != "Water")
                   .Where(resource => resource.Key != "Oxygen")
                   .ToDictionary(res => res.Key, res => res.Value);
                var test = planet.ResourceQuantity.Values.Max();
                List<string> maxResources = solidsDictionary
                    .Where(resource => resource.Value == solidsDictionary.Values.Max())
                    .Select(resource => resource.Key)
                    .ToList();
                if (solidsDictionary.Values.Max() > 0)
                {
                    if (maxResources.Count() == 1)
                    {
                        planet.ResourceQuantity[maxResources.First()]--;
                        owner.ResourcesQuantity[maxResources.First().ToLower()]++;
                    }
                    else if (maxResources.Contains("Titanium"))
                    {
                        planet.ResourceQuantity["Titanium"]--;
                        owner.ResourcesQuantity["titanium"]++;
                    }
                    else if (maxResources.Contains("Gold"))
                    {
                        planet.ResourceQuantity["Gold"]--;
                        owner.ResourcesQuantity["gold"]++;
                    }
                    else if (maxResources.Contains("Iron"))
                    {
                        planet.ResourceQuantity["Iron"]--;
                        owner.ResourcesQuantity["iron"]++;
                    }
                    else if (maxResources.Contains("Aluminium"))
                    {
                        planet.ResourceQuantity["Aluminium"]--;
                        owner.ResourcesQuantity["aluminium"]++;
                    }
                }
            }

        }

        internal bool BuilderHasNotMoved()
        {
            return mine.Planet == owner.GetBuilder().Planet;
        }
    }
}
