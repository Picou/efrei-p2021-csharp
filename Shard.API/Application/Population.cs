﻿using Shard.API.Application;
using Shard.Shared.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shard.API.Models
{
    public class Population
    {
        public List<User> Users { get; set; }

        private BattleManager battleManager;

        public Population(BattleManager battleManager)
        {
            Users = new List<User>();
            this.battleManager = battleManager;
        }

        // Empty constructor for test purpose
        public Population()
        {
            Users = new List<User>();
        }

        internal List<Unit> GetUnitsFromUser(string userId)
        {
            return Users.Find(user => user.Id.Equals(userId)).Units;
        }

        internal Unit GetUnitFromUser(string userId, string unitId)
        {
            return Users.Find(user => user.Id == userId).Units
                .Find(unit => unit.Id == unitId);
        }

        internal void UnitArriving(object state)
        {
            // set current location to destination after arriving
            Unit unitToModify = (Unit)state;
            unitToModify.Planet = unitToModify.DestinationPlanet;
            unitToModify.System = unitToModify.DestinationSystem;

            //check if there is any enemy unit to fight
            //if(unitToModify.IsBattleUnit() && EnemyShipIsHere(unitToModify))
            //{
            //    InitiateBattle(unitToModify);
            //}
        }

        private bool EnemyShipIsHere(Unit unit)
        {
            User owner = Users.Where(u => u.Units.Contains(unit))
                               .First();

            //exclude ships of owner
            List<User> otherUsers = Users;
            otherUsers.Remove(owner);
            List<Unit> enemyUnits = otherUsers.SelectMany(u => u.Units)
                .ToList();

            //check if any battle ships on the same planet 
            if (enemyUnits.Where(u => u.IsBattleUnit())
                .Where(u => u.System == unit.System)
                .Where(u => u.Planet == unit.Planet)
                .Count() > 0)
                return true;
            return false;
        }

        //TOFIX
        internal async Task<User> Authenticate(string username, string password)
        {
            if(username == "admin" && password == "password")
            {
                Users.Last().IsAdmin = true;
                return Users.Last();
            }
            throw new Exception();
        }

        internal void AddUserToBattle(User user)
        {
            this.battleManager.Users.Add(user);
        }

        //private void InitiateBattle(Unit unit)
        //{
        //    User owner = Users.Where(u => u.Units.Contains(unit))
        //                       .First();

        //    //exclude ships of owner
        //    List<User> otherUsers = Users;
        //    otherUsers.Remove(owner);
        //    List<Unit> enemyUnits = otherUsers.SelectMany(u => u.Units)
        //        .ToList();

        //    //get enemy battle ships on the same planet 
        //    enemyUnits = enemyUnits.Where(u => u.IsBattleUnit())
        //        .Where(u => u.System == unit.System)
        //        .Where(u => u.Planet == unit.Planet)
        //        .ToList();

        //    // initiate battle in a Task
        //    new BattleManager(clock,  unit, enemyUnits).InitiateBattle();
        //    // check every time unit inside battle manager if there are still units to fight

        //}





        internal void UpdateResources(User sentUser, User existingUser)
        {
            foreach(KeyValuePair<string, int> resource in sentUser.ResourcesQuantity)
            {
                if (sentUser.ResourcesQuantity.TryGetValue(resource.Key, out int value))
                    existingUser.ResourcesQuantity[resource.Key] = value;
            }
        }

        // determine how much ressources a unit costs
        // Attributing unit to a user
        // if it's a fighting unit, primarly adding it to the battlemanager of the planet
        internal bool TryQueueingUnit(User user, StarportItem starport, string type)
        {
            switch(type)
            {
                case "scout":
                    if (user.ResourcesQuantity["carbon"] >= 5 && user.ResourcesQuantity["iron"] >= 5)
                    {
                        string unitId = Guid.NewGuid().ToString();
                        Unit newScout = new Unit(unitId, type, starport.System, starport.Planet);
                        user.Units.Add(newScout);
                        user.ResourcesQuantity["carbon"] -= 5;
                        user.ResourcesQuantity["iron"] -= 5;
                    }
                    else
                        return false;
                    break;
                case "builder":
                    if (user.ResourcesQuantity["carbon"] >= 5 && user.ResourcesQuantity["iron"] >= 10)
                    {
                        string unitId = Guid.NewGuid().ToString();
                        Unit newBuilder = new Unit(unitId, type, starport.System, starport.Planet);
                        user.Units.Add(newBuilder);
                        user.ResourcesQuantity["carbon"] -= 5;
                        user.ResourcesQuantity["iron"] -= 10;
                    }
                    else
                        return false;
                    break;
                case "cruiser":
                    if (user.ResourcesQuantity["gold"] >= 20 && user.ResourcesQuantity["iron"] >= 60)
                    {
                        string unitId = Guid.NewGuid().ToString();
                        Unit newCruiser = new Unit(unitId, type, starport.System, starport.Planet);
                        user.Units.Add(newCruiser);
                        user.ResourcesQuantity["gold"] -= 20;
                        user.ResourcesQuantity["iron"] -= 60;
                        // adding user id to fighting unit for fighting purpose
                        newCruiser.UserId = user.Id;
                        //adding unit to battle manager 
                        battleManager.Units.Add(newCruiser);
                    }
                    else
                        return false;
                    break;
                case "fighter":
                    if (user.ResourcesQuantity["aluminium"] >= 10 && user.ResourcesQuantity["iron"] >= 20)
                    {
                        string unitId = Guid.NewGuid().ToString();
                        Unit newFighter = new Unit(unitId, type, starport.System, starport.Planet);
                        user.Units.Add(newFighter);
                        user.ResourcesQuantity["aluminium"] -= 10;
                        user.ResourcesQuantity["iron"] -= 20;
                        // adding user id to fighting unit for fighting purpose
                        newFighter.UserId = user.Id;
                        //adding unit to battle manager
                        battleManager.Units.Add(newFighter);
                    }
                    else
                        return false;
                    break;
                case "bomber":
                    if (user.ResourcesQuantity["titanium"] >= 10 && user.ResourcesQuantity["iron"] >= 30)
                    {
                        string unitId = Guid.NewGuid().ToString();
                        Unit newBomber = new Unit(unitId, type, starport.System, starport.Planet);
                        user.Units.Add(newBomber);
                        user.ResourcesQuantity["titanium"] -= 10;
                        user.ResourcesQuantity["iron"] -= 30;
                        // adding user id to fighting unit for fighting purpose
                        newBomber.UserId = user.Id;
                        //adding unit to battle manager
                        battleManager.Units.Add(newBomber);
                    }
                    else
                        return false;
                    break;
            }
            return true;
        }

        internal void AddUnitToUser(Unit unit, User user)
        {
            user.Units.Add(new Unit(unit));
        }
    }
}
