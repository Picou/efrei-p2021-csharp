﻿using Shard.API.Models;
using Shard.Shared.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shard.API.Application
{
    public class BattleManager
    {
        public List<Unit> Units { get; set; }
        public List<User> Users { get; set; }

        private Random rand;

        public BattleManager(IClock clock)
        {
            this.Units = new List<Unit>();
            this.rand = new Random();
            this.Users = new List<User>();
            // TODO should wait here to reach the first multiple of 6 and 60 before starting the timers
            clock.CreateTimer(FightEvery6s, null, TimeSpan.FromSeconds(0), TimeSpan.FromSeconds(6));
            clock.CreateTimer(FightEvery60s, null, TimeSpan.FromSeconds(0), TimeSpan.FromMinutes(1));
            clock.CreateTimer(RemoveDestroyedUnits, null, TimeSpan.FromSeconds(0), TimeSpan.FromSeconds(6));
        }

        private void RemoveDestroyedUnits(object state)
        {
            List<Unit> deadUnits = Units
                .Where(u => u.Health <= 0)
                .ToList();
            foreach(Unit unit in deadUnits)
            {
                foreach(User user in Users)
                {
                    if (user.Units.Contains(unit))
                        user.Units.Remove(unit);
                }
            }
        }

        private void FightEvery6s(object state)
        {
            if (Units.Count > 1)
            {

                foreach (Unit unit in Units)
                {
                    if (unit.Type != "bomber")
                    {
                        //get enemy battle ships on the same planet 
                        List<Unit> enemyUnits = Units
                            .Where(u => u.IsBattleUnit())
                            .Where(u => u.Id != unit.Id)
                            .Where(u => u.UserId != unit.UserId)
                            .Where(u => u.System == unit.System)
                            .Where(u => u.Planet == unit.Planet)
                            .ToList();

                        //get the target according to priority
                        Unit target = PickTarget(enemyUnits, false);

                        // inflict damage
                        if (unit.Type == "fighter")
                        {
                            target.Health -= 10;
                        }
                        else if (unit.Type == "cruiser")
                        {
                            for(int i = 0; i < 4; i++)
                            {
                                if (target.Health > 0)
                                    target.Health -= 10;
                                else
                                {
                                    enemyUnits.Remove(target);
                                    target = PickTarget(enemyUnits, false);
                                }
                            }
                        }
                    }

                }
            }
        }

        public void FightEvery60s(object state)
        {
            if (Units.Count > 1)
            {
                List<Unit> fightingUnits = Units
                    .Where(u => u.Type == "bomber")
                    .ToList();
                if(fightingUnits.Count() > 1)
                {
                    foreach (Unit unit in fightingUnits)
                    {
                        //get enemy battle ships on the same planet 
                        List<Unit> enemyUnits = fightingUnits
                            .Where(u => u.IsBattleUnit())
                            .Where(u => u.Id != unit.Id)
                            .Where(u => u.UserId != unit.UserId)
                            .Where(u => u.System == unit.System)
                            .Where(u => u.Planet == unit.Planet)
                            .ToList();
                        Unit target = PickTarget(enemyUnits, true);
                        target.Health -= 400;
                    }
                }
            }
        }

        private Unit PickTarget(List<Unit> enemyUnits, bool bomberPriority)
        {
            Unit target;
            if (bomberPriority)
            {
                //bomber priority
                if (enemyUnits
                    .Where(u => u.Type == "cruiser")
                    .Where(u => u.Health > 0)
                    .Count() > 0)
                {
                    int nbFighter = enemyUnits.Where(u => u.Type == "cruiser")
                        .Where(u => u.Health > 0)
                        .Count();
                    target = enemyUnits
                        .Where(u => u.Type == "cruiser")
                        .Where(u => u.Health > 0)
                        .ToList()[rand.Next(nbFighter)];
                }
                else if (enemyUnits
                    .Where(u => u.Type == "bomber")
                    .Where(u => u.Health > 0)
                    .Count() > 0)
                {
                    int nbCruiser = enemyUnits
                        .Where(u => u.Type == "bomber")
                        .Where(u => u.Health > 0)
                        .Count();
                    target = enemyUnits
                        .Where(u => u.Type == "bomber")
                        .Where(u => u.Health > 0)
                        .ToList()[rand.Next(nbCruiser)];
                }
                else
                {
                    // there only are fighters
                    int nbBomber = enemyUnits
                        .Where(u => u.Health > 0)
                        .Count();
                    target = enemyUnits
                        .Where(u => u.Health > 0)
                        .ToList()[rand.Next(nbBomber)];
                }
            }
            else
            {
                // normal priority
                if (enemyUnits.Where(u => u.Type == "fighter")
                    .Where(u => u.Health > 0)
                    .Count() > 0)
                {
                    int nbFighter = enemyUnits.Where(u => u.Type == "fighter")
                        .Where(u => u.Health > 0)
                        .Count();
                    target = enemyUnits
                        .Where(u => u.Type == "fighter")
                        .Where(u => u.Health > 0)
                        .ToList()[rand.Next(nbFighter)];
                }
                else if (enemyUnits.Where(u => u.Type == "cruiser")
                    .Where(u => u.Health > 0)
                    .Count() > 0)
                {
                    int nbCruiser = enemyUnits
                        .Where(u => u.Type == "cruiser")
                        .Where(u => u.Health > 0)
                        .Count();
                    target = enemyUnits
                        .Where(u => u.Type == "cruiser")
                        .Where(u => u.Health > 0)
                        .ToList()[rand.Next(nbCruiser)];
                }
                else
                {
                    // there only are bombers
                    int nbBomber = enemyUnits
                        .Where(u => u.Health > 0)
                        .Count();
                    target = enemyUnits
                        .Where(u => u.Health > 0)
                        .ToList()[rand.Next(nbBomber)];
                }
            }
            return target;
        }

    }
}
