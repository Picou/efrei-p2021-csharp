using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Shard.API.Application;
using Shard.API.Helpers;
using Shard.API.Models;
using Shard.Shared.Core;

namespace Shard.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            services.AddSingleton<Population>();
            services.AddSingleton<Constructions>();
            services.AddSingleton<BattleManager>();
            services.AddSingleton<IClock, Shared.Core.SystemClock>();
                //services.Configure<MapGeneratorOptions>(
                //    Configuration.GetSection("MapGeneratorOptions"));
            MapGeneratorOptions options = new MapGeneratorOptions();
            options.Seed = "Test application";
            MapGenerator mapGenerator = new MapGenerator(options);
            services.AddSingleton(mapGenerator.Generate());

            services.AddCors();
            services.AddAuthentication("Basic")
             .AddScheme<AuthenticationSchemeOptions, BasicAuthenticationHandler>("Basic", null);

            //var client = new MongoClient("");
            //client.GetDatabase("").GetCollection<>("").Find...
            //cr�er un singleton qui va g�rer la connexion � la base � partir d'une classe d'option (connexion string, nom de la db)
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseCors(x => x
                .AllowCredentials());

            app.UseAuthentication();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
