﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Shard.API.Utils
{
    public class Utils
    {
        public static bool IsCorrectId(string id)
        {
            Regex regex = new Regex("^[a-zA-Z0-9_-]*$");
            return regex.IsMatch(id);
        }
    }
}
