﻿using Shard.Shared.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shard.API.Models
{
    public class SectorItem
    {
        public List<SystemItem> Systems { get; }

        public SectorItem(SectorSpecification sectorSpec)
        {
            Systems = new List<SystemItem>();
            foreach (SystemSpecification systemSpec in sectorSpec.Systems)
            {
                Systems.Add(new SystemItem(systemSpec));
            }
        }

        internal PlanetItem GetPlanet(string planet)
        {
            foreach (var p in from SystemItem s in Systems
                              from PlanetItem p in s.Planets
                              where p.Name == planet
                              select p)
            {
                return p;
            }

            return null;
        }
    }
}
