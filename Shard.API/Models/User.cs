﻿using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace Shard.API.Models
{
    public class User
    {
        public bool IsAdmin { get; set; }
        public string Id { get; set; }
        public string Pseudo { get; set; }
        public DateTime DateOfCreation { get; set; }
        public IDictionary<string, int> ResourcesQuantity { get; set; }
        [JsonIgnore]
        public List<Unit> Units { get; set; }

        internal User()
        {
            DateOfCreation = DateTime.Now;
            Units = new List<Unit>();

            //adding default user resources
            ResourcesQuantity = new Dictionary<string, int>
            {
                { "aluminium", 0 },
                { "carbon", 20 },
                { "gold", 0 },
                { "iron", 10 },
                { "oxygen", 50 },
                { "titanium", 0 },
                { "water", 50 }
            };
            IsAdmin = false;
        }

        internal Unit GetBuilder()
        {
            return Units.Find(unit => unit.Type == "builder");
        }
    }
}