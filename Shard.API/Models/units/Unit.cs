﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading;
using System.Threading.Tasks;
using Shard.Shared.Core;

namespace Shard.API.Models
{
    public class Unit
    {

        public Unit() 
        {
            this.BuildCanceller = new CancellationTokenSource();
        }

        public Unit(Unit unit)
        {
            Id = unit.Id;
            Type = unit.Type;
            System = unit.System;
            SetHealthAccordingToType();
        }

        public Unit(string type, SystemSpecification unitSystem/*, PlanetSpecification unitPlanet*/)
        {
            this.Id = Guid.NewGuid().ToString();
            this.Type = type;
            this.System = unitSystem.Name;
            this.Planet = null;//unitPlanet.Name;
            SetHealthAccordingToType();


            this.BuildCanceller = new CancellationTokenSource();
        }

        public Unit(string id, string type, string systemId, string planetId)
        {
            Id = id;
            Type = type;
            Planet = planetId;
            System = systemId;
            SetHealthAccordingToType();


            this.BuildCanceller = new CancellationTokenSource();
        }

        private void SetHealthAccordingToType()
        {
            switch (Type)
            {
                case "fighter":
                    Health = 80;
                    break;
                case "bomber":
                    Health = 50;
                    break;
                case "cruiser":
                    Health = 400;
                    break;
            }
        }

        internal bool IsBattleUnit()
        {
            if (Type == "fighter" || Type == "bomber" || Type == "cruiser")
                return true;
            return false;
        }

        //might need to separate builder, scout and fighter as only the builder uses cancellationtoken
        [JsonIgnore]
        public CancellationTokenSource BuildCanceller { get; set; }
        [JsonIgnore]
        public string UserId { get; set; }

        public string Id { get; set; } 
        public string Type { get; set; } 
        public string System { get; set; } 
        public string Planet { get; set; }
        public string DestinationSystem { get; set; }
        public string DestinationPlanet { get; set; }
        public DateTime EstimatedTimeOfArrival { get; set; }
        public int Health { get; set; }

    }
}
