﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shard.API.Models.units
{
    public class ReceivedForQueuing
    {
        public string Type { get; set; }
    }
}
