﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shard.API.Models.units
{
    public enum UnitKind
    {
        scout,
        builder,
        cruiser,
        fighter,
        bomber
    }
}
