﻿using Shard.Shared.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shard.API.Models
{
    public class SystemItem
    {
        public string Name { get; }
        public List<PlanetItem> Planets { get; }

        internal SystemItem(SystemSpecification systemSpecification)
        {
            Name = systemSpecification.Name;

            Planets = new List<PlanetItem>();
            foreach (PlanetSpecification planetSpec in systemSpecification.Planets)
            {
                Planets.Add(new PlanetItem(planetSpec));
            }
        }
    }
}
