﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shard.API.Models
{
    public abstract class BuildingItem
    {
        public string Id { get; set; }
        public string Type { get; set; }
        public string BuilderId { get; set; }
        public string System { get; set; }
        public string Planet { get; set; }
        public bool IsBuilt { get; set; }
        public string ResourceCategory { get; set; }
        public DateTime? EstimatedBuildTime { get; set; }

        public BuildingItem() 
        {
            EstimatedBuildTime = null;
        }

        public BuildingItem(string type, string builderId, string systemId, string planetId, DateTime builtDate)
        {
            Id = Guid.NewGuid().ToString();
            Type = type;
            BuilderId = builderId;
            System = systemId;
            Planet = planetId;
            IsBuilt = false;
            EstimatedBuildTime = builtDate;
        }
    }
}
