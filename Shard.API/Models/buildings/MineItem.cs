﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shard.API.Models
{
    public class MineItem : BuildingItem
    {

        public MineItem(string type, string builderId, string systemId, string planetId, string resourceCategory, DateTime builtDate) : base(type, builderId, systemId, planetId, builtDate)
        {
            ResourceCategory = resourceCategory;
        }
    }
}
