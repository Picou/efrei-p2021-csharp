﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shard.API.Models
{
    public class StarportItem : BuildingItem
    {
        public StarportItem(string type, string builderId, string systemId, string planetId, DateTime builtDate) : base(type, builderId, systemId, planetId, builtDate)
        {
            ResourceCategory = null;
        }
    }
}
