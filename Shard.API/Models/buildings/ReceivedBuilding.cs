﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shard.API.Models.buildings
{
    public class ReceivedBuilding
    {
        public string BuilderId { get; set; }
        public string Type { get; set; }
        public string ResourceCategory { get; set; }
    }
}
