﻿using Shard.API.Application;
using Shard.Shared.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Shard.API.Models
{
    public class PlanetItem
    {
        public string Name { get; }
        public int Size { get; }
        [JsonIgnore]
        public IDictionary<string, int> ResourceQuantity { get; set; }

        internal PlanetItem(PlanetSpecification planetSpec)
        {
            Name = planetSpec.Name;
            Size = planetSpec.Size;
            ResourceQuantity = new Dictionary<string, int>();
            //Seems impossible to get enum value as key using ToDictionary on IReadOnlyDictionary
            ResourceQuantity = planetSpec.ResourceQuantity.ToDictionary(kvp => kvp.Key.ToString(), kvp => kvp.Value);
        }
    }
}
