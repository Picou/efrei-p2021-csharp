﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Shard.Shared.Core;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;

namespace Shard.API.Models
{
    public class Location
    {
        public string System { get; }
        public string Planet { get; }
        public IDictionary<string, int> ResourcesQuantity { get; }

        public Location(Unit unit, SectorSpecification sector)
        {
            this.ResourcesQuantity = null;
            this.System = unit.System;
            this.Planet = unit.Planet;
            SystemSpecification systemSpec = sector.Systems.SingleOrDefault(system => system.Name == unit.System);
            PlanetSpecification planetSpec = systemSpec.Planets.SingleOrDefault(planet => planet.Name == unit.Planet);
            
            if(unit.Type == "scout")
                ResourcesQuantity = planetSpec.ResourceQuantity.ToDictionary(item => item.Key.ToString().ToLower(), item => item.Value);
        }

    }
}
