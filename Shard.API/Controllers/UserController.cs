﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Shard.API.Application;
using Shard.API.Models;
using Shard.Shared.Core;

namespace Shard.API.Controllers
{
    [Route("[controller]s")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly Population population;
        private readonly SectorSpecification sectorSpecification;
        private readonly Random random;

        public UserController(Population population, SectorSpecification sectorSpecification)
        {
            this.population = population;
            this.sectorSpecification = sectorSpecification;
            this.random = new Random();
        }

        // GET: Users/5
        [HttpGet("{id}")]
        public ActionResult<User> Get(string id)
        {
            var user = this.population.Users.SingleOrDefault(item => item.Id == id);
            if (user == null)
                return NotFound();

            return user;
        }

        // PUT: Users/5
        [HttpPut("{id}")]
        public ActionResult<User> Put(string id, [FromBody] User user)
        {
            if (id != user.Id || user == null || !Utils.Utils.IsCorrectId(id))
                return BadRequest();

            // if user already exists
            foreach (User existingUser in population.Users)
            {
                if (existingUser.Id == id)
                {
                    if(existingUser.IsAdmin)
                        population.UpdateResources(user, existingUser);

                    return existingUser;
                }
            }

            //Using random system and planets to generate scout and builder units
            SystemSpecification unitSystem = sectorSpecification.Systems[random.Next(sectorSpecification.Systems.Count)];
            PlanetSpecification unitPlanet = unitSystem.Planets[random.Next(unitSystem.Planets.Count)];
            user.Units.Add(new Unit("scout", unitSystem/*, unitPlanet*/));
            unitPlanet = unitSystem.Planets[random.Next(unitSystem.Planets.Count)];
            user.Units.Add(new Unit("builder", unitSystem/*, unitPlanet*/));
            population.Users.Add(user);
            population.AddUserToBattle(user);

            return user;

        }
    }
}
