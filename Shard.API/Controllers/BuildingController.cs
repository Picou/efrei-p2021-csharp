﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Shard.API.Models;
using Shard.API.Models.buildings;
using Shard.API.Models.units;
using Shard.Shared.Core;

namespace Shard.API.Controllers
{
    [Route("users")]
    [ApiController]
    public class BuildingController : ControllerBase
    {
        private readonly Population population;
        private readonly SectorItem sector;
        private readonly Constructions constructions;
        private readonly IClock clock;

        public BuildingController(Population population, SectorSpecification sector, Constructions constructions, IClock clock)
        {
            this.population = population;
            this.sector = new SectorItem(sector);
            this.constructions = constructions;
            this.clock = clock;
        }

        // POST: users/userId/Buildings
        [HttpPost("{userId}/Buildings")]
        public ActionResult<BuildingItem> Post(string userId, [FromBody] ReceivedBuilding building)
        {
            List<string> buildingKinds = new List<string>(new string[] { "mine", "starport" });
            User user = this.population.Users.SingleOrDefault(item => item.Id == userId);

            if (user == null)
                return NotFound();

            if (building == null || building.Type == null || !buildingKinds.Contains(building.Type) || building.ResourceCategory == null)
                return BadRequest();

            Unit builder = user.Units.SingleOrDefault(unit => unit.Id == building.BuilderId);

            if (builder == null || builder.Planet == null)
                return BadRequest();


            DateTime builtDate = clock.Now.AddMinutes(5);
            constructions.AddABuilding(building, builder, builtDate);

            var addedBuilding = constructions.GetABuilding(building.Type, builder.System, builder.Planet);
            if (building.Type == "mine")
            {
                constructions.BuildMineAfterTime(addedBuilding, sector.GetPlanet(builder.Planet), user, clock);
            }
            else if(building.Type == "starport")
            {
                constructions.BuildStarportAfterTime(addedBuilding as StarportItem, clock);
            }

            return constructions.GetABuilding(building.Type, builder.System, builder.Planet);
        }

        // GET: users/userId/Buildings
        [HttpGet("{userId}/Buildings")]
        public ActionResult<List<BuildingItem>> GetAllBuildingsFromUser(string userId)
        {
            User user = this.population.Users.SingleOrDefault(item => item.Id == userId);

            if (user == null)
                return NotFound();

            List<BuildingItem> buildings = constructions.GetBuildingsOfUser(user);

            if (buildings == null)
                return NotFound();

            return buildings;
        }

        // GET: users/userId/Buildings/buildingId
        [HttpGet("{userId}/Buildings/{buildingId}")]
        public async Task<ActionResult<BuildingItem>> GetBuildingFromUserAsync(string userId, string buildingId)
        {

            User user = this.population.Users.SingleOrDefault(item => item.Id == userId);

            if (user == null)
                return NotFound();

            BuildingItem building = constructions.GetABuilding(user, buildingId);

            if (building == null)
                return NotFound();

            //get cancellation token from builder unit
            CancellationToken token = population.GetUnitFromUser(userId, building.BuilderId).BuildCanceller.Token;
            token.ThrowIfCancellationRequested();

            if (building.EstimatedBuildTime != null)
            {
                TimeSpan beforeBuilt = building.EstimatedBuildTime.GetValueOrDefault().Subtract(clock.Now);
                if (beforeBuilt > TimeSpan.Zero && beforeBuilt <= TimeSpan.FromSeconds(2))
                {
                    beforeBuilt = building.EstimatedBuildTime.GetValueOrDefault().Subtract(clock.Now);
                    try
                    {
                        await clock.Delay(beforeBuilt, token);
                    }
                    catch(OperationCanceledException)
                    {
                        return NotFound();
                    }
                }
            }

            return building;
        }

        // POST: users/userId/Buildings/starportId/queue
        [HttpPost("{userId}/Buildings/{starportId}/queue")]
        public ActionResult<Unit> QueueingUnit(string userId, string starportId, [FromBody] ReceivedForQueuing type)
        {
            if (!Enum.GetNames(typeof(UnitKind)).Contains(type.Type))
                return BadRequest();

            User user = this.population.Users.SingleOrDefault(item => item.Id == userId);

            if (user == null)
                return NotFound();

            BuildingItem starport = this.constructions.GetABuilding(user, starportId);

            if (starport == null)
                return NotFound();

            if (starport.Type != "starport" || starport.IsBuilt == false)
                return BadRequest();

            if (!population.TryQueueingUnit(user, (StarportItem)starport, type.Type))
                return BadRequest();

            return user.Units.Last();

        }
    }
}
