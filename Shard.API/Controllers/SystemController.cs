﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Shard.API.Models;
using Shard.Shared.Core;


namespace Shard.API.Controllers
{
    [Route("[controller]s")]
    public class SystemController : Controller
    {
        private readonly SectorItem sector;

        public SystemController(SectorSpecification sectorSpecification, IClock clock)
        {
            //reproduction du sector avec des données alégées pour ce controlleur
            this.sector = new SectorItem(sectorSpecification);
        }

        // GET: systems
        [HttpGet]
        public IReadOnlyList<SystemItem> GetSystems()
        {
            return this.sector.Systems;
        }

        // GET systems/5
        [HttpGet("{name}")]
        public ActionResult<SystemItem> GetSystem(string name)
        {
            var system = this.sector.Systems.SingleOrDefault(item => item.Name.Equals(name));
            if (system == null)
                return NotFound();

            return system;

        }

        // GET systems/5/planets
        [HttpGet("{name}/planets")]
        public ActionResult<IReadOnlyList<PlanetItem>> GetPlanets(string name)
        {
            var system = this.sector.Systems.SingleOrDefault(item => item.Name.Equals(name));
            if (system == null)
                return NotFound();

            return system.Planets.ToList();
        }

        // GET systems/5/planets/6
        [HttpGet("{namesystem}/planets/{nameplanet}")]
        public async Task<ActionResult<PlanetItem>> GetPlanet(string nameSystem, string namePlanet)
        {
            var system = this.sector.Systems.SingleOrDefault(item => item.Name.Equals(nameSystem));
            if (system == null)
                return NotFound();

            var planet = system.Planets.SingleOrDefault(item => item.Name.Equals(namePlanet));
            if (planet == null)
                return NotFound();

            return await Task.Run(() => planet);
        }

    }
}
