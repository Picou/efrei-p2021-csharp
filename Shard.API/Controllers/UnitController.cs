﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Shard.API.Models;
using Shard.Shared.Core;

namespace Shard.API.Controllers
{
    [Route("users")]
    [ApiController]
    public class UnitController : ControllerBase
    {
        private readonly Population population;
        private readonly SectorSpecification sectorSpecification;
        private readonly IClock clock;
        private readonly Constructions constructions;

        public UnitController(Population population, SectorSpecification sectorSpecification, IClock clock, Constructions constructions)
        {
            this.population = population;
            this.sectorSpecification = sectorSpecification;
            this.clock = clock;
            this.constructions = constructions;
        }

        // GET: users/{userId}/Units
        [HttpGet("{userId}/Units")]
        public ActionResult<List<Unit>> GetUnits(string userId)
        {
            var user = this.population.Users.SingleOrDefault(item => item.Id == userId);
            if (user == null)
                return NotFound();

            return population.GetUnitsFromUser(userId);
        }

        // GET: users/{userId}/Units/{unitId}
        [HttpGet("{userId}/Units/{unitId}")]
        public async Task<ActionResult<Unit>> GetUnitAsync(string userId, string unitId)
        {
            Unit unit = population.GetUnitFromUser(userId, unitId);
            if (unit == null)
                return NotFound();

            TimeSpan beforeArrival = unit.EstimatedTimeOfArrival.Subtract(clock.Now);
            if (beforeArrival > TimeSpan.Zero && beforeArrival <= TimeSpan.FromSeconds(2))
            {
                beforeArrival = unit.EstimatedTimeOfArrival.Subtract(clock.Now);
                await clock.Delay(beforeArrival);
            }
            return unit;

        }

        // GET: users/{userId}/Units/{unitId}/location
        [HttpGet("{userId}/Units/{unitId}/location")]
        public ActionResult<Location> GetLocationAsync(string userId, string unitId)
        {
            Unit unit = population.GetUnitFromUser(userId, unitId);
            if (unit == null)
                return NotFound();

            return new Location(unit, sectorSpecification);
        }

        // PUT: users/{userId}/Units/{unitId}
        [HttpPut("{userId}/Units/{unitId}")]
        public ActionResult<Unit> Put(string userId, string unitId, [FromBody] Unit unit)
        {
            DateTime estimatedArrival = clock.Now;

            var user = this.population.Users.SingleOrDefault(item => item.Id == userId);
            if (user == null)
                return NotFound();

            Unit unitToModify = population.GetUnitFromUser(userId, unitId);

            if (unitToModify == null)
            {
                population.AddUnitToUser(unit, user);
                return unit;
            }
            else
            {

                if (unitId != unit.Id || unit == null)
                    return BadRequest();

                SystemItem currentSystem = new SectorItem(sectorSpecification).Systems.Find(system => system.Name == unitToModify.System);


                if (unit.DestinationPlanet != null)
                    estimatedArrival = estimatedArrival.AddSeconds(15);
                if (unit.DestinationSystem != null && currentSystem != null && unit.DestinationSystem != currentSystem.Name)
                    estimatedArrival = estimatedArrival.AddSeconds(60);

                string departingPlanet = unitToModify.Planet;
                string departingSystem = unitToModify.System;

                unitToModify.System = null;
                unitToModify.Planet = null;
                unitToModify.DestinationSystem = unit.DestinationSystem;
                unitToModify.DestinationPlanet = unit.DestinationPlanet;
                unitToModify.EstimatedTimeOfArrival = estimatedArrival;
                clock.CreateTimer(population.UnitArriving, unitToModify, (long)(estimatedArrival - clock.Now).TotalMilliseconds, Timeout.Infinite);

                //cancel building if being built
                if (unitToModify.Type == "builder" && departingPlanet != unitToModify.DestinationPlanet)
                    constructions.DeleteCurrentlyBuilt(unit, departingPlanet, departingSystem);
            }

            return unitToModify;
        }
    }
}
