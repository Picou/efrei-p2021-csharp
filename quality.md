# Qualité

* Nommage variables: 1
* Usage adéquat des fonctionnalités du langage: 0.8
* Simplicité des fonctions: 0.5
* Simplicité des classes: 1
* Pas de dupplication de code: 1
* Séparation de responsabilités: 1
* Encapsulation: 0.5
