using Microsoft.AspNetCore.Mvc.Testing;
using Shard.API;
using Shard.Shared.Web.IntegrationTests;
using System;
using Xunit;

namespace Shard.Tests
{
    public class IntegrationTests : BaseIntegrationTests<Startup, WebApplicationFactory<Startup>>
    {
        public IntegrationTests(WebApplicationFactory<Startup> factory) : base(factory)
        {
        }
    }
}
