﻿using Shard.API.Utils;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace Shard.Tests
{
    public class UtilsTests
    {
        [Theory]
        [InlineData("aaaaaa")]
        [InlineData("01234567")]
        [InlineData("ABABAB0101")]
        [InlineData("ababBAB0101")]
        [InlineData("aB_01")]
        [InlineData("aB_01-_-")]
        public void IsCorrectId(string id)
        {
            Assert.True(Utils.IsCorrectId(id));
        }

        [Theory]
        [InlineData("?aaa0A")]
        [InlineData("!aaa0A")]
        [InlineData("'aaa")]
        [InlineData("éaaa")]
        [InlineData("()")]
        [InlineData("ça&")]
        public void IsIncorrectId(string id)
        {
            Assert.False(Utils.IsCorrectId(id));
        }
    }
}
