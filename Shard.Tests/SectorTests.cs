﻿using Shard.API.Models;
using Shard.Shared.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace Shard.Tests
{
    public class SectorTests
    {

        public SectorItem CreateSector()
        {
            return new SectorItem(
                    new MapGenerator(
                        new MapGeneratorOptions())
                            .Generate());
        }

        [Fact]
        public void Create_FromSectorSpecification_SectorContentNotNull()
        {
            Assert.NotNull(CreateSector());
        }

        [Fact]
        public void Create_FromSectorSpecification_SystemsContentNotNull()
        {
            Assert.NotNull(CreateSector().Systems);
        }

        [Fact]
        public void Create_FromSectorSpecification_PlanetsContentNotNull()
        {
            Assert.False(CreateSector().Systems?.Any() == null);
        }
    }
}
