﻿using Shard.API.Models;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace Shard.Tests
{
    public class PopulationTests
    {
        [Fact]
        public void Create_FromDefaultConstructor_UsersNotNull()
        {
            Population population = new Population();
            Assert.NotNull(population.Users);
        }
    }
}
